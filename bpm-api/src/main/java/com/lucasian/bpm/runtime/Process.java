package com.lucasian.bpm.runtime;

public interface Process {

	String getId();
	
	boolean isEnded();
	
}
