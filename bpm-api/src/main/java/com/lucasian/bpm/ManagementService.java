package com.lucasian.bpm;

public interface ManagementService {

	void deployFile(String filepath);
	
	void deployResource(String classpath);
	
}
